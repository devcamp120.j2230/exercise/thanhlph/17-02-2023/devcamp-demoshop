import { Component } from "react";

class FooterComponent extends Component {
    render() {
        return (
            <footer className="text-center text-lg-start" style={{ backgroundColor: "rgb(45,50,70)" }}>

                <div className="container p-4">

                    <div className="row">

                        <div className="col-lg-5 col-md-6 mb-4 mb-md-0">
                            <p className="text-white" style={{ marginLeft: "150px" }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>

                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <h5 className="text-white">Contacts</h5>

                            <ul className="list-unstyled">
                                <li>
                                    <h6 className="text-white mt-2"><b>Address:</b></h6>
                                    <h7 className="text-white">Kolkata, West Bengal, India</h7>
                                </li>
                                <li>
                                    <h6 className="text-white mt-2"><b>Email:</b></h6>
                                    <h7 className="text-white">info@example.com</h7>
                                </li>
                                <li>
                                    <h6 className="text-white mt-2"><b>Phone:</b></h6>
                                    <h7 className="text-white">+91 99999999 or +91 11111111</h7>
                                </li>
                            </ul>
                        </div>

                        <div className="col-lg-4 col-md-6 mb-4 mb-md-0">
                            <h5 className="text-white">Links</h5>

                            <ul className="list-unstyled mb-0">
                                <li>
                                    <a href="#!" className="text-white mt-2" style={{ textDecoration: "none" }}>About</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-white mt-2" style={{ textDecoration: "none" }}>Projects</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-white mt-2" style={{ textDecoration: "none" }}>Blog</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-white mt-2" style={{ textDecoration: "none" }}>Contacts</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-white mt-2" style={{ textDecoration: "none" }}>Pricing</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div className="text-center p-3" style={{ backgroundColor: "rgb(45,50,70)" }}>
                    <p classNameName="text-white">DemoShop24h.com</p>
                </div>
                <div className='row'>
                    <div className="col-sm-3 text-center text-white" style={{ border: "0.1px solid white" }}>
                        <p style={{ margin: "10px auto" }}><b>FACEBOOK</b></p>
                    </div>
                    <div className="col-sm-3 text-center text-white" style={{ border: "0.1px solid white" }}>
                        <p style={{ margin: "10px auto" }}><b>INSTAGRAM</b></p>
                    </div>
                    <div className="col-sm-3 text-center text-white" style={{ border: "0.1px solid white" }}>
                        <p style={{ margin: "10px auto" }}><b>TWITTER</b></p>
                    </div>
                    <div className="col-sm-3 text-center text-white" style={{ border: "0.1px solid white" }}>
                        <p style={{ margin: "10px auto" }}><b>GOOGLE</b></p>
                    </div>
                </div>
            </footer>
        )
    }
}

export default FooterComponent;