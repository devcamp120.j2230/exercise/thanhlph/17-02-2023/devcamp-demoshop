import data from "../../../data/ProductData.json"
import "../../content/content.css"
function CardDeck2() {
    return(
        <div className="div-card-row">
            <div className="card data-card">
                <div className="card-header">
                    <h3 className="text-primary text-center">
                        {data.Products[3].Title}
                    </h3>
                </div>
                <div className="div-img">
                    <img className="card-img" src={data.Products[0].ImageUrl} alt="img" />
                </div>
                <div className="card-body">
                    <p>{(data.Products[3].Description).substring(0, 100)} {(data.Products[3].Description).length >= 100 && '...'}</p>
                    <p><b>Category:</b> {data.Products[3].Category}</p>
                    <p><b>Made by:</b> {data.Products[3].Manufacturer}</p>
                    <p><b>Organic:</b> {data.Products[3].Organic == true ? "Yes" : "No"}</p>
                    <p><b>Price:</b> ${data.Products[3].Price}</p>
                </div>
                <div className="d-flex justify-content-center mb-3">
                    <button className="btn btn-primary">Add To Cart</button>
                </div>
            </div>

            <div className="card data-card">
                <div className="card-header">
                    <h3 className="text-primary text-center">
                        {data.Products[4].Title}
                    </h3>
                </div>
                <div className="div-img">
                    <img className="card-img" src={data.Products[4].ImageUrl} alt="img" />
                </div>
                <div className="card-body">
                    <p>{(data.Products[4].Description).substring(0, 100)} {(data.Products[4].Description).length >= 100 && '...'}</p>
                    <p><b>Category:</b> {data.Products[4].Category}</p>
                    <p><b>Made by:</b> {data.Products[4].Manufacturer}</p>
                    <p><b>Organic:</b> {data.Products[4].Organic == true ? "Yes" : "No"}</p>
                    <p><b>Price:</b> ${data.Products[4].Price}</p>
                </div>
                <div className="d-flex justify-content-center mb-3">
                    <button className="btn btn-primary">Add To Cart</button>
                </div>
            </div>

            <div className="card data-card">
                <div className="card-header">
                    <h3 className="text-primary text-center">
                        {data.Products[5].Title}
                    </h3>
                </div>
                <div className="div-img">
                    <img className="card-img" src={data.Products[5].ImageUrl} alt="img" />
                </div>
                <div className="card-body">
                    <p>{(data.Products[5].Description).substring(0, 100)} {(data.Products[5].Description).length >= 100 && '...'}</p>
                    <p><b>Category:</b> {data.Products[5].Category}</p>
                    <p><b>Made by:</b> {data.Products[5].Manufacturer}</p>
                    <p><b>Organic:</b> {data.Products[5].Organic == true ? "Yes" : "No"}</p>
                    <p><b>Price:</b> ${data.Products[5].Price}</p>
                </div>
                <div className="d-flex justify-content-center mb-3">
                    <button className="btn btn-primary">Add To Cart</button>
                </div>
            </div>
        </div>
    )
}

export default CardDeck2;