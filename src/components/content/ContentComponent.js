import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import CardDeck1 from "../content/cardDeck1/cardDeck1"
import CardDeck2 from "./cardDeck2/cardDeck2";
import CardDeck3 from "./cardDeck3/cardDeck3";

class ContentComponent extends Component {
    render() {
        return (
            <div>
                <div className="container">
                    <div>
                        <h3>Product List</h3>
                        <p>Showing 1 - 9 of 24 products</p>
                    </div>
                    <>
                        <CardDeck1 />
                    </>
                    <>
                        <CardDeck2 />
                    </>
                    <>
                        <CardDeck3 />
                    </>
                </div>
            </div>

        )
    }
}

export default ContentComponent;