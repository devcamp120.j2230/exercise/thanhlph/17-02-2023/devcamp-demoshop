import data from "../../../data/ProductData.json"
import "../../content/content.css"
function CardDeck3() {
    return(
        <div className="div-card-row">
            <div className="card data-card">
                <div className="card-header">
                    <h3 className="text-primary text-center">
                        {data.Products[6].Title}
                    </h3>
                </div>
                <div className="div-img">
                    <img className="card-img" src={data.Products[6].ImageUrl} alt="img" />
                </div>
                <div className="card-body">
                    <p>{(data.Products[6].Description).substring(0, 100)} {(data.Products[6].Description).length >= 100 && '...'}</p>
                    <p><b>Category:</b> {data.Products[6].Category}</p>
                    <p><b>Made by:</b> {data.Products[6].Manufacturer}</p>
                    <p><b>Organic:</b> {data.Products[6].Organic == true ? "Yes" : "No"}</p>
                    <p><b>Price:</b> ${data.Products[6].Price}</p>
                </div>
                <div className="d-flex justify-content-center mb-3">
                    <button className="btn btn-primary">Add To Cart</button>
                </div>
            </div>

            <div className="card data-card">
                <div className="card-header">
                    <h3 className="text-primary text-center">
                        {data.Products[7].Title}
                    </h3>
                </div>
                <div className="div-img">
                    <img className="card-img" src={data.Products[7].ImageUrl} alt="img" />
                </div>
                <div className="card-body">
                    <p>{(data.Products[7].Description).substring(0, 100)} {(data.Products[7].Description).length >= 100 && '...'}</p>
                    <p><b>Category:</b> {data.Products[7].Category}</p>
                    <p><b>Made by:</b> {data.Products[7].Manufacturer}</p>
                    <p><b>Organic:</b> {data.Products[7].Organic == true ? "Yes" : "No"}</p>
                    <p><b>Price:</b> ${data.Products[7].Price}</p>
                </div>
                <div className="d-flex justify-content-center mb-3">
                    <button className="btn btn-primary">Add To Cart</button>
                </div>
            </div>

            <div className="card data-card">
                <div className="card-header">
                    <h3 className="text-primary text-center">
                        {data.Products[8].Title}
                    </h3>
                </div>
                <div className="div-img">
                    <img className="card-img" src={data.Products[8].ImageUrl} alt="img" />
                </div>
                <div className="card-body">
                    <p>{(data.Products[8].Description).substring(0, 100)} {(data.Products[8].Description).length >= 100 && '...'}</p>
                    <p><b>Category:</b> {data.Products[8].Category}</p>
                    <p><b>Made by:</b> {data.Products[8].Manufacturer}</p>
                    <p><b>Organic:</b> {data.Products[8].Organic == true ? "Yes" : "No"}</p>
                    <p><b>Price:</b> ${data.Products[8].Price}</p>
                </div>
                <div className="d-flex justify-content-center mb-3">
                    <button className="btn btn-primary">Add To Cart</button>
                </div>
            </div>
        </div>
    )
}

export default CardDeck3;