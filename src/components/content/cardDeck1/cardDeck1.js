import data from "../../../data/ProductData.json"
import "../../content/content.css"
function CardDeck1() {
    return(
        <div className="div-card-row">
            <div className="card data-card">
                <div className="card-header">
                    <h3 className="text-primary text-center">
                        {data.Products[0].Title}
                    </h3>
                </div>
                <div className="div-img">
                    <img className="card-img" src={data.Products[0].ImageUrl} alt="img" />
                </div>
                <div className="card-body">
                    <p>{(data.Products[0].Description).substring(0, 100)} {(data.Products[0].Description).length >= 100 && '...'}</p>
                    <p><b>Category:</b> {data.Products[0].Category}</p>
                    <p><b>Made by:</b> {data.Products[0].Manufacturer}</p>
                    <p><b>Organic:</b> {data.Products[0].Organic == true ? "Yes" : "No"}</p>
                    <p><b>Price:</b> ${data.Products[0].Price}</p>
                </div>
                <div className="d-flex justify-content-center mb-3">
                    <button className="btn btn-primary">Add To Cart</button>
                </div>
            </div>

            <div className="card data-card">
                <div className="card-header">
                    <h3 className="text-primary text-center">
                        {data.Products[1].Title}
                    </h3>
                </div>
                <div className="div-img">
                    <img className="card-img" src={data.Products[1].ImageUrl} alt="img" />
                </div>
                <div className="card-body">
                    <p>{(data.Products[1].Description).substring(0, 100)} {(data.Products[1].Description).length >= 100 && '...'}</p>
                    <p><b>Category:</b> {data.Products[1].Category}</p>
                    <p><b>Made by:</b> {data.Products[1].Manufacturer}</p>
                    <p><b>Organic:</b> {data.Products[1].Organic == true ? "Yes" : "No"}</p>
                    <p><b>Price:</b> ${data.Products[1].Price}</p>
                </div>
                <div className="d-flex justify-content-center mb-3">
                    <button className="btn btn-primary">Add To Cart</button>
                </div>
            </div>

            <div className="card data-card">
                <div className="card-header">
                    <h3 className="text-primary text-center">
                        {data.Products[2].Title}
                    </h3>
                </div>
                <div className="div-img">
                    <img className="card-img" src={data.Products[2].ImageUrl} alt="img" />
                </div>
                <div className="card-body">
                    <p>{(data.Products[2].Description).substring(0, 100)} {(data.Products[2].Description).length >= 100 && '...'}</p>
                    <p><b>Category:</b> {data.Products[2].Category}</p>
                    <p><b>Made by:</b> {data.Products[2].Manufacturer}</p>
                    <p><b>Organic:</b> {data.Products[2].Organic == true ? "Yes" : "No"}</p>
                    <p><b>Price:</b> ${data.Products[2].Price}</p>
                </div>
                <div className="d-flex justify-content-center mb-3">
                    <button className="btn btn-primary">Add To Cart</button>
                </div>
            </div>
        </div>
    )
}

export default CardDeck1;