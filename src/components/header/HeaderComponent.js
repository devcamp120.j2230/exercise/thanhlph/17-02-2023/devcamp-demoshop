import { Component } from "react";
import logoImage from "../../assets/images/logo.svg"

class HeaderComponent extends Component {
    render() {
        return (
            <div>
                <div className="d-flex flex-row container mt-2 align-items-center">
                    <div className="col-3">
                        <div>
                            <img src={logoImage} alt="logo" width={60} />
                        </div>
                    </div>
                    <div className="col-6 text-center">
                        <h1>React Store</h1>
                        <p>Demo App Shop24h v1.0</p>
                    </div>
                </div>
                <div className="bg-dark" style={{ padding: "20px" }}>
                    <a style={{ color: "white", fontWeight: "bold", textDecoration: "none" }} href="/">HOME</a>
                </div>
            </div>
        )
    }
}

export default HeaderComponent;