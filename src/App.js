import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import HeaderComponent from "../src/components/header/HeaderComponent";
import FooterComponent from './components/footer/FooterComponent';
import ContentComponent from './components/content/ContentComponent';

function App() {
  return (
    <div>
      <HeaderComponent />
      <ContentComponent />
      <div>
        <FooterComponent />
      </div>
    </div>

  );
}

export default App;
